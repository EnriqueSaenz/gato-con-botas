package ejercicio01;

//import necesario para la clase Scanner
import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		//declaro objeto Scanner
		Scanner lector = new Scanner(System.in);
		
		//pido los datos al usuario
		System.out.println("Dame un n�mero entero");
		//leo el dato que he pedido
		int numero=lector.nextInt();
		
		//calculo el doble
		int doble =numero*2;
		//lo muestro
		System.out.println("El doble es "+doble);
		
		//calculo y muestro de vez
		System.out.println("El triple es "+(3*numero));
		
		//cierro scanner
		lector.close();
		

	}

}
