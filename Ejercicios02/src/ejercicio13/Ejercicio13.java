package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena1=input.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2=input.nextLine();
		
		System.out.println(cadena2.contains(cadena1)?
				"Si esta contenida"
				:"No esta contenida");
		
		input.close();
		

	}

}
