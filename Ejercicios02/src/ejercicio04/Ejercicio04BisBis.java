package ejercicio04;

import java.util.Scanner;

public class Ejercicio04BisBis {
	
	//creo una constante que tiene el numero pi
	static final double NUMERO_PI=3.1416;

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce el radio de la circunferencia");
		double radio=lector.nextDouble();
		
		double area=radio*radio*NUMERO_PI;
		double longitud=2*NUMERO_PI*radio;
		
		System.out.println("Longitud de la circunferencia "+longitud);
		System.out.println("Area de la circunferencia "+area);
		
		lector.close();
	}

}
