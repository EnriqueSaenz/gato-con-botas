package ejerciciosprevios;

public class EjerciciosPrevios {

	public static void main(String[] args) {
		//Math.sqrt(numero) calcula la raiz cuadrada
		System.out.println(Math.sqrt(4));
		
		String cadena1="Hola";
		String cadena2="Caracola";
		//replaceAll
		String cadena3=cadena1.replaceAll("a", "aaaaaaaaa");
		System.out.println("cadena1 "+cadena1);
		System.out.println("cadena2 "+cadena2);
		System.out.println("cadena3 "+cadena3);
			
		//equals
		String cadena4="Hola";
		System.out.println("cadena1 y cadena2 iguales? "+cadena1.equals(cadena2));
		System.out.println("cadena1 y cadena4 iguales? "+cadena1.equals(cadena4));
		
		//compareTo
		//Un int <0, 0, � >0 si el par�metro es menor, igual o mayor a la cadena.
		System.out.println("Comparo cadena1 y cadena2 "+cadena1.compareTo(cadena2));
		
		//muestro la primera letra
		System.out.println("Primera letra de cadena1 "+cadena1.charAt(0));
		
		//contains
		String cadena5="Hola caracola";
		//true si una cadena contiene a la otra
		System.out.println(cadena5.contains(cadena4));
		//false si una cadena no contiene a la otra
		System.out.println(cadena1.contains(cadena2));
		
		//indexOf
		//devuelve la posici�n comenzando por 0
		System.out.println(cadena5.indexOf('H'));
		//devuelve la primera ocurrencia
		System.out.println(cadena5.indexOf('o'));
		
		//startsWith
		//comprueba si comienza por una cadena (String)
		System.out.println(cadena5.startsWith("H"));
		System.out.println(cadena5.startsWith("ola"));
	
		//endsWith
		//comprueba si finaliza por una cadena (String)
		System.out.println(cadena5.endsWith("ola"));
		System.out.println(cadena5.endsWith("tora"));

	}

}
