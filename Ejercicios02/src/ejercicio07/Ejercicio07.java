package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero (3 cifras)");
		String cadena=input.nextLine();
		
		//dividir en cifras siendo un String
		//tengo que usar charAt
		System.out.println(cadena.charAt(0));
		System.out.println(cadena.charAt(1));
		System.out.println(cadena.charAt(2));
		
		System.out.println("Introduce nuevamente un numero (3 cifras)");
		int numero=input.nextInt();
		
		//dividir en cifras siendo un int
		//tengo que usar division y resto
		System.out.println(numero/100);
		System.out.println((numero/10)%10);
		System.out.println(numero%10);
		
		
		input.close();

	}

}
